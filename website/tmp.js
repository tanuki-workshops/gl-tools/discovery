https://docs.gitlab.com/ee/api/job_artifacts.html

let projects = data.filter(item => item.mergeRequests.length > 0 && item.jobs.length > 0 && item.name_with_namespace.includes("cse-dt"))

let simpleProjectsList = projects.map(project => {
	return {
		id: project.id,
		name: project.name,
		mergeRequests: project.mergeRequests,
		jobs: project.jobs,
		
	}
})


letGetAllMrJobs = project => {
	
	mrList = []
	
	project.mergeRequests.forEach(mr => {
		
		let lastJobLinkedToMR = project.jobs.find(job => {
			return job.commit.id == mr.merge_commit_sha 
		})
		
		
		let mergeRequest = {
			id: mr.id, 
			iid: mr.iid, 
			source_branch: mr.source_branch, 
			merge_commit_sha: mr.merge_commit_sha, 
			title: mr.title, 
			merge_status: mr.merge_status,
			job: {
				artifacts: lastJobLinkedToMR != undefined ? lastJobLinkedToMR.artifacts : [],
				id: lastJobLinkedToMR != undefined ? lastJobLinkedToMR.id : undefined
			}
			
		}
		
		mrList.push(mergeRequest)
		
		
	})
	
	return mrList
	
}


let mrJobsProjectsList = simpleProjectsList.map(project => {
	return {
		id: project.id,
		name: project.name,
		mergeRequests: letGetAllMrJobs(project)
	}

})


mrJobsProjectsList.forEach(project => {
	console.log(project.name)
	project.mergeRequests.forEach(mr => {
		mr.job.artifacts.forEach(artifact => {
			console.log(" - ", artifact.filename)
		})
	})
})





