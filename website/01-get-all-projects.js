const DEBUG = process.env.DEBUG || false

const GLClient = require("./libs/gl-cli").GLClient
const fs = require("fs")
const jsonexport = require('jsonexport');


async function getAllProjects({glClient, perPage, page}) {
  return glClient.getAllProjects({perPage, page})
}

async function getAllGroups({glClient, perPage, page}) {
  return glClient.getAllGroups({perPage, page})
}

// November 2th 2022
async function getAllDescendantGroups({glClient, groupId, perPage, page}) {
  return glClient.getAllDescendantGroups({groupId, perPage, page})
}

async function getAllProjectsOfGroup({glClient, groupId, perPage, page}) {
  return glClient.getAllProjectsOfGroup({groupId, perPage, page})
}

async function getProjectVulnerabilityFindings({glClient, projectId, perPage, page}) {
  return glClient.getProjectVulnerabilityFindings({projectId, perPage, page})
}

async function getProjectJobs({glClient, projectId, perPage, page}) {
  return glClient.getProjectJobs({projectId, perPage, page})
}





async function getProjectLanguages({glClient, projectId}) {
  return glClient.getProjectLanguages({projectId})
}

async function getProjectMergeRequests({glClient, projectId}) {
  return glClient.getProjectMergeRequests({projectId})
}

async function getProjectMembers({glClient, projectId}) {
  return glClient.getProjectMembers({projectId}) 
}




/**
 * Get all projects of the GitLab instance
 * @param {*} glClient 
 */
async function getInstanceProjects(glClient) {
  try {
    var stop = false, page = 1, projects = []
    while (!stop) {
      await getAllProjects({glClient, perPage:20, page:page}).then(someProject => {
        
        projects = projects.concat(someProject.map(project => {
          return project
        }))
        if(someProject.length == 0) { stop = true }
      })
      page +=1
    }
    return projects
  } catch (error) {
    console.log("😡:", error)
  }
}

/**
 * Get all groups  of the GitLab instance
 * @param {*} glClient 
 */
async function getInstanceGroups(glClient) {
  // 5085244
  try {
    var stop = false, page = 1, groups = []
    while (!stop) {
      await getAllGroups({glClient, perPage:20, page:page}).then(someGroup => {
        groups = groups.concat(someGroup.map(group => {
          return group
        }))
        if(someGroup.length == 0) { stop = true }
      })
      page +=1
    }
    return groups
  } catch (error) {
    console.log("😡:", error)
  }
}




// TODO: sort the records
// TODO: add the record to a SQLite db?
// https://jsoneditoronline.org/#left=local.xizire
async function getOrganizationGroups(glClient, groupId) {
  try {
    var stop = false, page = 1, groups = []
    while (!stop) {
      await getAllDescendantGroups({glClient, groupId: groupId, perPage:20, page:page}).then(someGroup => {
        groups = groups.concat(someGroup.map(group => {
          return group
        }))
        if(someGroup.length == 0) { stop = true }
      })
      page +=1
    }
    return groups
  } catch (error) {
    console.log("😡:", error)
  }
}

async function getProjectsOfGroup(glClient, groupId) {
  try {
    var stop = false, page = 1, projects = []
    while (!stop) {
      await getAllProjectsOfGroup({glClient, groupId, perPage:20, page:page}).then(groupProjects => {
        //console.log("🖐", groupProjects)
        
        projects = projects.concat(groupProjects)
        
        if(groupProjects.length == 0) { stop = true }
      })
      page +=1
    }
    //console.log("🖐", projects)
    return projects
  } catch (error) {
    console.log("😡:", error)
  }
}

/*
async function getProjectVulnerabilityFindings({glClient, projectId, perPage, page}) {
  return glClient.getProjectVulnerabilityFindings({projectId, perPage, page})
}

*/

async function getProjectVulnerabilities({glClient, projectId}) {
  try {
    var stop = false, page = 1, vulnerabilities = []
    while (!stop) {
      await getProjectVulnerabilityFindings({glClient, projectId: projectId, perPage:20, page:page}).then(projectVulnerabilities => {

        vulnerabilities = vulnerabilities.concat(projectVulnerabilities)


        if(projectVulnerabilities.length == 0) { stop = true }
      })
      page +=1
    }
    return vulnerabilities
  } catch (error) {
    console.log("😡:", error)
  }
}

async function getProjectCIJobs({glClient, projectId}) {
  try {
    var stop = false, page = 1, jobs = []
    while (!stop) {
      await getProjectJobs({glClient, projectId: projectId, perPage:20, page:page}).then(projectJobs => {

        jobs = jobs.concat(projectJobs)


        if(projectJobs.length == 0) { stop = true }
      })
      page +=1
    }
    return jobs
  } catch (error) {
    console.log("😡:", error)
  }
}




async function batch(gitLabClient, groupId) {

  //await getInstanceProjects(gitLabClient).then(res => projectsGLInstance=res)
  //await getInstanceGroups(gitLabClient).then(res => groupsGLInstance=res)

  console.log("⏳ get the groups tree...")
  var organizationGroups
  await getOrganizationGroups(gitLabClient, groupId).then(res => organizationGroups=res)

  // For each group find the project list


  fs.writeFileSync(`./all_groups.json`, JSON.stringify(organizationGroups))

  /*
  jsonexport(organizationGroups, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./all_groups.csv`, csv)
  }) 
  */

  //getProjectsOfGroup(glClient, groupId)

  console.log("⏳ get all the projects of all the groups...")

  var allProjectsGroups = []

  for (const currentGroup of organizationGroups) {
    var projectsGroup
    console.log("Group ->", currentGroup.id, currentGroup.web_url)

    await getProjectsOfGroup(gitLabClient, currentGroup.id).then(res => projectsGroup=res)

    allProjectsGroups = allProjectsGroups.concat(projectsGroup)

  }

  console.log("⏳ get statistics of all the projects...")
  var projectsCounter =0
  for (const currentProject of allProjectsGroups) {

    await getProjectLanguages({glClient: gitLabClient, projectId: currentProject.id}).then(languages => {
      currentProject.languages = languages
    })

    await getProjectMembers({glClient: gitLabClient, projectId: currentProject.id}).then(members => {
      currentProject.members = members
    })

    await getProjectVulnerabilities({glClient: gitLabClient, projectId: currentProject.id}).then(vulnerabilities => {
      currentProject.vulnerabilities = vulnerabilities
    })


    await getProjectMergeRequests({glClient: gitLabClient, projectId: currentProject.id}).then(mergeRequests => {
      currentProject.mergeRequests = mergeRequests
    })

    
    await getProjectCIJobs({glClient: gitLabClient, projectId: currentProject.id}).then(jobs => {
      currentProject.jobs = jobs
    })
    

    console.log(projectsCounter, "Project ->", currentProject.id, currentProject.name, currentProject.languages)
    console.log("  mergeRequests->", currentProject.mergeRequests.length)
    console.log("  jobs->", currentProject.jobs)

    projectsCounter+=1


  }

  fs.writeFileSync(`./all_projects_groups.json`, JSON.stringify(allProjectsGroups))

  /*
  jsonexport(allProjectsGroups, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./all_projects_groups.csv`, csv)
  })  
  */

  return {message:"done"}

}


let gitLabClient = new GLClient({
  baseUri: `${process.env.URL}/api/v4`,
  token: process.env.TOKEN
})

let gitLabOrganizationGroupId = process.argv[2]

batch(gitLabClient, gitLabOrganizationGroupId).then(result => {
  console.log(result)
})

